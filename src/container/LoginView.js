import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import { loginUserAction, registerUserAction } from '../actions/sessionActions';
import { setCookie, deleteSessionCookies, checkCookie } from '../utils/cookies';

class LoginView extends Component {

  constructor(props) {

    super(props);

    this.state = {
      email: '',
      password: '',
      register: false,
      submitted: false

    };
  }

  validateForm() {
    return this.state.email.length > 0 && this.state.password.length > 0;
  }

  handleCheckboxChange (event) {

    const target = event.target;
    const name = target.name;
    const value = target.checked;

    this.setState({
      ...this.state,
      register: value
    });
  }

  handleChange (event) {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

  handleSubmit (event) {
    event.preventDefault();

    let email = event.target.email.value;
    let password = event.target.password.value;

    const data = {
      email, password
    };

    const dispatchMethod = this.state.register ? registerUserAction : loginUserAction;
    this.props.dispatch(dispatchMethod(data));

  }

  getCurrentLabel(register) {
    return !!register ? "Rejestracja" : "Logowanie";
  }

  render() {

    const { email, password, submitted, register } = this.state;
    let isSuccess, message;

    let loginOrRegister = this.state.register ? 'register' : 'login';

    /*
    * Obsługa odpowiedzi serwisu
    * */
    if (!!this.props.response && this.props.response[loginOrRegister].hasOwnProperty('response')) {

      const response = this.props.response[loginOrRegister].response;

      if(response) {
        isSuccess = response.status === 200;
        message = response.message;

        if (isSuccess && !!response.data.session_id) {

          setCookie('session_id', response.data.session_id, 1);
          setCookie('session_key', response.data.session_key, 1);

        }
      }


    }
    if(!isSuccess) {
      deleteSessionCookies();
    }


    return(
        isSuccess
            ? <Redirect to="/notes" /> :
        <div className="col-md-6 col-md-offset-3">
          <h2>{this.getCurrentLabel(register)}</h2>
          <form onSubmit={this.handleSubmit.bind(this)}>
            <div className={'form-group' + (submitted && !email ? ' has-error' : '')}>
              <label htmlFor="email">Email</label>
              <input type="text" className="form-control" name="email"
                     onChange={this.handleChange.bind(this)} />
              {submitted && !email &&
              <div className="help-block">Podaj adres email!</div>
              }
            </div>
            <div className={'form-group' + (submitted && !password ? ' has-error' : '')}>
              <label htmlFor="password">Hasło</label>
              <input type="password" className="form-control" name="password"
                     onChange={this.handleChange.bind(this)} />
              {submitted && !password &&
              <div className="help-block">Password is required</div>
              }
            </div>
            <div className="form-check">
              <input type="checkbox" onChange={this.handleCheckboxChange.bind(this)}
                     className="form-check-input" name="register" value={register}/>
              <label className="form-check-label" htmlFor="register">Nowy użytkownik</label>
            </div>
            <div className="form-group">
              <button className="btn btn-primary" type="submit">{this.getCurrentLabel(register)}</button>
            </div>
          </form>
        </div>

    );
  }
}

const mapStateToProps = (response) => ({response});

export default connect(mapStateToProps)(LoginView);