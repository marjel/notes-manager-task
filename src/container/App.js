import React, { Component } from 'react';

import {
  BrowserRouter,
  Route,
  Switch
} from 'react-router-dom';

import LoginView from './LoginView';
import NotesView from "./NotesView";

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div>
          <Switch>
            <Route path='/' exact={true} component={LoginView} />
            <Route path='/login' component={LoginView} />
            <Route path='/notes' component={NotesView} />
          </Switch>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;