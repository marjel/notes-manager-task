import React, {Component} from 'react';
import {deleteSessionCookies} from '../utils/cookies';
import {Redirect} from "react-router-dom";
import {connect} from 'react-redux';

import { createNoteAction, readNotesAction, updateNoteAction, deleteNoteAction } from '../actions/notesActions';
import Link from "react-router-dom/es/Link";

const VOLUME = 10;

const logoutBarLeftStyle = {
    float: 'left',
    padding: '8px'
};
const logoutBarRightStyle = {
    float: 'right',
    padding: '8px'
};

class NotesView extends Component {

    componentDidMount() {
        this.props.dispatch(readNotesAction({after: null, volume: VOLUME}));
    }

    constructor(props) {
        super(props);
        this.state = {
            redirect: false,
            notes: []
        };
    }

    handleLogout(event) {
        event.preventDefault();
        deleteSessionCookies();
        this.setState({
            ...this.state,
            redirect: true
        });
    }

    handleCreate(event) {
        event.preventDefault();
    }

    render() {
        /*
        * Obsługa odpowiedzi serwisu
        * */

        let notesData, notes;
        if (!!this.props.response && this.props.response.notes.hasOwnProperty('response')) {
            const response = this.props.response.notes.response;

            if(response.status === 200) {
                notesData = response.data;
                notes = <div>


                </div>;
            }
        }



        return(
            this.state.redirect ? <Redirect to="/login" /> :
            <div><button style={logoutBarLeftStyle} className={'btn-primary'}
                         onClick={this.handleCreate.bind(this)}>Utwórz notatkę</button>
                <button onClick={this.handleLogout.bind(this)}
                        style={logoutBarRightStyle} className={'btn-default'}>logout</button>{notes}</div>

        );
    }

}

const mapStateToProps = (response) => ({response});

export default connect(mapStateToProps)(NotesView)