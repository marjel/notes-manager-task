import * as types from '../actions';

export default function(state = [], action) {
  const response = action.response;

  switch(action.type) {
    case types.CREATE_NOTE_SUCCESS:
      return { ...state, response };
    case types.CREATE_NOTE_ERROR:
      return { ...state, response };
    case types.READ_NOTES_SUCCESS:
      return { ...state, response };
    case types.READ_NOTES_ERROR:
      return { ...state, response };
    case types.UPDATE_NOTE_SUCCESS:
      return { ...state, response };
    case types.UPDATE_NOTE_ERROR:
      return { ...state, response };
    case types.DELETE_NOTE_SUCCESS:
      return { ...state, response };
    case types.DELETE_NOTE_ERROR:
      return { ...state, response };
    default:
      return state;
  }
};