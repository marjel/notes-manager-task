import { combineReducers } from 'redux';
import register from './registerReducer';
import login from './loginReducer';
import notes from './noteReducer';

const rootReducer = combineReducers({
  register, login, notes
});

export default rootReducer;