import { CapiClient,
    UserSignup,
    SessionCreate,
    SessionInfo,
    NoteList,
    NoteTake
} from '../services/rekrClient';
import {getCookie} from "../utils/cookies";

const client = new CapiClient('https://rekrutacja.sandbox-m.sowa.pl/api/v1');

function getSessionIdKeyObject() {

    const sessionId = getCookie('session_id');
    const sessionKey = getCookie('session_key');

    return {sessionId, sessionKey};

}

/*
* User session
* */
export const registerUserService = (request) => {
  return client
      .executeSingle(new UserSignup(request.user.email, request.user.password))
      .then(response => {
          debugger;
          if(response.status === 200){
              return client
                  .withLogin(request.user.email, request.user.password)
                  .executeSingle(new SessionCreate());
          } else {
              throw new Error(response.message);
          }

      })

};

export const loginUserService = (request) => {
    return client
        .withLogin(request.user.email, request.user.password)
        .executeSingle(new SessionCreate());
};

export const sessionInfoService = (request) => {
    return client
        .withSession(request.session.session_id, request.session.session_key)
        .executeSingle(new SessionInfo());
};

/*
* Note CRUD
* */

export const createNoteService = (request) => {

    const {sessionId, sessionKey} = getSessionIdKeyObject();

    return client
        .withSession(sessionId, sessionKey)
        .executeSingle(new NoteTake(request.note.name, request.note.text));
};

export const readNotesByPageService = (request) => {

    const {sessionId, sessionKey} = getSessionIdKeyObject();

    return client
        .withSession(sessionId, sessionKey)
        .executeSingle(new NoteList(request.page));
};

export const updateNoteService = createNoteService;

/*
* Notatki usunięte są flagowane za pomocą 'del' - wynik moze byc filtrowany przed wyswietlaniem albo elementy usunięte
* mogą być wyróżnine w widoku
*
* */
export const deleteNoteService = (request) => {

    return createNoteService('del', 'del');
};