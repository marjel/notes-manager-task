import * as types from './index';

export const createNoteAction = (note) => {
  return {
    type: types.CREATE_NOTE,
    note
  }
};

export const readNotesAction = (page) => {
  return {
    type: types.READ_NOTES,
    page
  }
};

export const updateNoteAction = (note) => {
  return {
    type: types.UPDATE_NOTE,
    note
  }
};

export const deleteNoteAction = (note) => {
  return {
    type: types.DELETE_NOTE,
    note
  }
};

