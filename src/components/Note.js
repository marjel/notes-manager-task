import React, { Component } from 'react';

const nameStyle = {
    fontSize: '13px',
    fontWeight: 'bold'
};
const textStyle = {
    fontSize: '11px'
};

class Note extends Component {

    handleDelete(event) {
        event.preventDefault();

    }

    handleSave(event) {
        event.preventDefault();;
    }

    render() {

        return(
                <div>
                    <p style={nameStyle}>{this.props.title}</p>
                    <p style={textStyle}>{this.props.text}</p>
                    <button className="btn btn-danger" onClick={this.handleDelete.bind(this)}>usuń notatkę</button>
                    <button className="btn btn-primary" disabled={true} onClick={this.handleSave.bind(this)}>
                        zapisz zmiany</button>
                </div>

        );
    }

}