import { takeLatest } from 'redux-saga/effects';
import { registerSaga, loginSaga } from './sessionSaga';
import {createNoteSaga, deleteNoteSaga, readNoteSaga, updateNoteSaga} from "./notesSaga";
import * as types from '../actions';

export default function* watchAppData() {
  yield takeLatest(types.REGISTER_USER, registerSaga);
  yield takeLatest(types.LOGIN_USER, loginSaga);
  yield takeLatest(types.CREATE_NOTE, createNoteSaga);
  yield takeLatest(types.READ_NOTES, readNoteSaga);
  yield takeLatest(types.UPDATE_NOTE, updateNoteSaga);
  yield takeLatest(types.DELETE_NOTE, deleteNoteSaga);
}