import { fork } from 'redux-saga/effects';
import watchAppData from './watchers';

export default function* getSagas() {
  yield fork(watchAppData);
}