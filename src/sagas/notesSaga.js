import { put, call } from 'redux-saga/effects';
import {createNoteService, deleteNoteService, readNotesByPageService, updateNoteService} from '../services/dataService';

import * as types from '../actions';

export function* createNoteSaga(payload) {
  try {
    const response = yield call(createNoteService, payload);
    yield [
      put({ type: types.CREATE_NOTE_SUCCESS, response })
    ];
  } catch(error) {
    yield put({ type: types.CREATE_NOTE_ERROR, error })
  }
}
export function* readNoteSaga(payload) {
  try {
    const response = yield call(readNotesByPageService, payload);
    yield [
      put({ type: types.READ_NOTES_SUCCESS, response })
    ];
  } catch(error) {
    yield put({ type: types.READ_NOTES_ERROR, error });
  }
}

export function* updateNoteSaga(payload) {
  try {
    const response = yield call(updateNoteService, payload);
    yield [
      put({ type: types.UPDATE_NOTE_SUCCESS, response })
    ];
  } catch(error) {
    yield put({ type: types.UPDATE_NOTE_ERROR, error })
  }
}

export function* deleteNoteSaga(payload) {
  try {
    const response = yield call(deleteNoteService, payload);
    yield [
      put({ type: types.DELETE_NOTE_SUCCESS, response })
    ];
  } catch(error) {
    yield put({ type: types.DELETE_NOTE_ERROR, error })
  }
}