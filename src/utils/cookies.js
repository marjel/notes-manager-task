export function setCookie(name, value, hours) {
  let d = new Date();
  d.setTime(d.getTime() + (hours * 60 * 60 * 1000));
  let expires = 'expires=' + d.toUTCString();
  document.cookie = name + '=' + value + ';' + expires + ';path=/';
}

export function getCookie(name) {
  let n = name + '=';
  let ca = document.cookie.split(';');
  
  for(let i = 0; i < ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) ===' ') {
      c = c.substring(1);
    }
    if (c.indexOf(n) === 0) {
      return c.substring(n.length, c.length);
    }
  }

  return '';
}

export function checkCookie() {
  let session = getCookie('session_id');
  if (session !== '') {
    return session;
  } else {
    return null;
  }

}

export function deleteCookie(name) {
  document.cookie = name+'=; Max-Age=-99999999;';
}

export function deleteSessionCookies() {
  deleteCookie('session_id');
  deleteCookie('session_key');
}